/*
 * This program is designed to use the lua libraries to build a 
 * framework.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STD_BUF_LEN 256
#define PROG_PURPOSE "| This program is built to be a simple lua parser\n"\
                     "| and will be used as the baseline to drive an\n"\
                     "| extensible game framework, written in c and\n"\
                     "| driven by lua.\n"

char *get_header_string( char *aString, int aRequestedLen, int aMaxLength )
{
  int i = 0;
  for( ; i < aMaxLength - 1 && i < aRequestedLen; i++ )
    {
      aString[i] = '-';
    }
  aString[i] = '\0';
  return aString;
}

void print_program_header(int argc, char **argv)
{
  int i = 0;
  int lProgramLength = strlen(*argv);
  static char lBuffer[STD_BUF_LEN];
  get_header_string( lBuffer, lProgramLength + 2, STD_BUF_LEN );

  // Print the program name.
  printf( "+%s+\n", lBuffer );
  printf( "| %s |\n", *argv );
  printf( "+%s+\n", lBuffer );

  for( ; i < argc; i++ )
    {
      printf( "| argv[%d] '%s'\n", i, argv[i] );
    }

  printf( "+%s+\n", lBuffer );

  printf( PROG_PURPOSE );

  printf( "+%s+\n", lBuffer );
}

int main(int argc, char **argv)
{
  print_program_header( argc, argv );
  
  return EXIT_SUCCESS;
}
